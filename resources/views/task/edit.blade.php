@extends('layouts.main')
@section('content')
<div class="w-75">
    <h3>Task #{{ $task->id }} Editing Form</h3>
    <form action="{{ route('tasks.update', $task) }}" method="POST">
        @csrf
        @method('PATCH')
        <div class="row mb-3">
            <div class="col-8">
                <label for="title" class="form-label">Title</label>
                <input
                    type="text"
                    name="title"
                    class="form-control @error('title') is-invalid @enderror"
                    value="{{ $task->title }}"
                >
                @error('title')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-4">
                <label class="form-label" for="deadline">Deadline</label>
                <input
                    type="datetime-local"
                    name="deadline_at"
                    class="form-control @error('deadline_at') is-invalid @enderror"
                    value="{{ $task->deadline_at }}"
                >
                @error('deadline_at')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Status</label>
            <select class="form-select form-select-sm w-25" name="status">
                @foreach($statuses as $status)
                    <option class="@statusClass($status->value)"
                        value="{{ $status->value }}"
                        @if($task->status === $status) selected @endif
                    >{{ $status->toReadable() }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea
                type="text"
                name="description"
                class="form-control @error('description') is-invalid @enderror"
            >{{ $task->description }}</textarea>
            @error('description')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary w-25">Update</button>
    </form>
</div>
@endsection
