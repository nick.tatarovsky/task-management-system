@extends('layouts.main')
@section('content')
<table class="table mt-4">
    <thead>
    <tr>
        <th scope="col" class="w-50">Title</th>
        <th scope="col">Status</th>
        <th scope="col">Deadline</th>
        <th scope="col" class="text-center">
            <a href="{{ route('tasks.create') }}">Create task</a>
        </th>
    </tr>
    </thead>
    <tbody>
        @foreach ($tasks as $task)
        <tr>
            <td>
                <a href="{{ route('tasks.show', $task) }}">
                    {{ $task->title }}
                </a>
            </td>
            <td>
                <b class="@statusClass($task->status->value)">
                    {{ $task->status->toReadable() }}
                </b>
            </td>
            <td>
                <span>{{ formatDatetime($task->deadline_at) }}</span>
            </td>
            <td>
                <div class="row">
                    <div class="col text-end">
                        <a href="{{ route('tasks.edit', $task) }}" class="btn btn-warning">
                            <img src="{{ asset('images/form/pencil.svg') }}" alt="Edit">
                        </a>
                    </div>
                    <div class="col">
                        <form action="{{ route('tasks.delete', $task) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                                <img src="{{ asset('images/form/trash.svg') }}" alt="Delete">
                            </button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@if(! $tasks->count())
    <p>There are no tasks yet...</p>
@endif
{{ $tasks->withQueryString()->links() }}
@endsection
