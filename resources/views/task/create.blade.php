@extends('layouts.main')
@section('content')
<div class="w-75">
    <form action="{{ route('tasks.store') }}" method="POST">
        @csrf
        <div class="row mb-3">
            <div class="col-8">
                <label for="title" class="form-label">Title</label>
                <input
                    type="text"
                    name="title"
                    class="form-control @error('title') is-invalid @enderror"
                    value="{{ old('title') }}"
                >
                @error('title')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-4">
                <label class="form-label" for="deadline">Deadline</label>
                <input
                    type="datetime-local"
                    name="deadline_at"
                    class="form-control @error('deadline_at') is-invalid @enderror"
                    value="{{ old('deadline_at') }}"
                >
                @error('deadline_at')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea
                type="text"
                name="description"
                class="form-control @error('description') is-invalid @enderror"
            >{{ old('description') }}</textarea>
            @error('description')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary w-25">Create</button>
    </form>
</div>
@endsection
