@extends('layouts.main')
@section('content')
    <div>
        <h3 class="mt-4">
            <a href="{{ route('tasks.edit', $task) }}" class="btn btn-warning">
                <img src="{{ asset('images/form/pencil.svg') }}" alt="Edit">
            </a>
            Task #{{ $task->id }}
        </h3>
        <div class="row">
            <div class="col-8">
                <label for="title">Title</label><br>
                <small class="text-muted">{{ $task->title }}</small>
            </div>
            <div class="col-2">
                <label for="title">Deadline</label><br>
                <small class="text-muted">{{ formatDatetime($task->deadline_at) }}</small>
            </div>
            <div class="col-2">
                <label for="title">Status</label><br>
                <small class="text-muted">{{ $task->status->toReadable() }}</small>
            </div>
        </div>
        <div class="col-8 mt-3">
            <label for="title">Description</label><br>
            <small class="text-muted">{{ $task->description }}</small>
        </div>
    </div>
@endsection
