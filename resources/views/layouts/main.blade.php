<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>

    @vite(['resources/js/app.js'])
    @vite(['resources/css/app.css'])
</head>
<body>
    <div class="container main-container preloader">
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">Task manager</a>
                <div class="navbar-collapse">
                    @auth
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('tasks.index') }}">Tasks</a>
                            </li>
                            <li class="nav-item">
                                <form action="{{ route('auth.logout' ) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="nav-link">Logout</button>
                                </form>
                            </li>
                        </ul>
                        <form action="{{ route('tasks.index') }}" class="d-flex" role="search">
                            <input
                                name="title"
                                class="form-control me-2"
                                type="search"
                                placeholder="Search by title"
                                aria-label="Search"
                                value="{{ request()->input('title') }}"
                            >
                            <button class="btn btn-outline-success" type="submit">Search</button>
                        </form>
                        @auth
                            <p class="mb-0 ms-4">{{ Auth::user()?->name }}</p>
                        @endauth
                    @else
                        <ul class="navbar-nav mb-2 mb-lg-0 ms-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('auth.register') }}">Sign Up</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('auth.login') }}">Log In</a>
                            </li>
                        </ul>
                    @endauth
                </div>
            </div>
        </nav>
        @if(session('success'))
            <div class="alert alert-success mt-2 mb-2" id="flash-message">
                {{ session('success') }}
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger mt-2 mb-2" id="flash-message">
                {{ session('error') }}
            </div>
        @endif
        @yield('content')
    </div>
</body>
</html>

<script>
    setTimeout(function() {
        let flashMessage = document.getElementById('flash-message');

        if (flashMessage) {
            flashMessage.style.display = 'none';
        }
    }, 3000);
</script>
