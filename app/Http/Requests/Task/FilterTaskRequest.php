<?php

declare(strict_types=1);

namespace App\Http\Requests\Task;

use App\Enums\Task\TaskStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class FilterTaskRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => 'nullable|string|max:255',
            'status' => ['nullable', 'int', new Enum(TaskStatus::class)],
            'per_page' => 'nullable|int',
            'page' => 'nullable|int',
        ];
    }
}
