<?php

declare(strict_types=1);

namespace App\Http\Requests\Task;

use App\Enums\Task\TaskStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'status' => [
                'nullable',
                'int',
                new Enum(TaskStatus::class),
            ],
            'title' => [
                'nullable',
                'string',
                'max:255',
                Rule::unique('tasks')->ignore($this->task)->where(function ($query) {
                    return $query->where('user_id', Auth::id());
                }),
            ],
            'description' => 'nullable|string',
            'deadline_at' => 'nullable|date|after:tomorrow',
        ];
    }
}
