<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\InvalidCredentialsException;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\AuthService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AuthController extends Controller
{
    public function __construct(
        private readonly AuthService $authService
    ) {}

    public function loginForm(): View
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request): RedirectResponse
    {
        $credentials = $request->validated();

        try {
            $this->authService->login($credentials);

            return redirect()->route('tasks.index');
        } catch (InvalidCredentialsException $e) {
            return redirect()->route('auth.loginForm')->withErrors([
                'auth-fail' => $e->getMessage()
            ]);
        }
    }

    public function logout(): RedirectResponse
    {
        $this->authService->logout();

        return redirect()->route('auth.login');
    }

    public function registerForm(): View
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request): RedirectResponse
    {
        $this->authService->register($request->validated());

        return redirect()->route('tasks.index');
    }
}
