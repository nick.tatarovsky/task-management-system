<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\Task\TaskStatus;
use App\Http\Requests\Task\FilterTaskRequest;
use App\Http\Requests\Task\StoreTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Models\Task;
use App\Services\TaskService;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TaskController extends Controller
{
    public function __construct(
        private readonly TaskService $taskService
    ) {
        //
    }

    public function index(FilterTaskRequest $request): View
    {
        $tasks = $this->taskService->getList($request->validated());

        return view('task.index', compact('tasks'));
    }

    public function create(): View
    {
        return view('task.create');
    }

    public function store(StoreTaskRequest $request): RedirectResponse
    {
        try {
            $this->taskService->store($request->validated());

            return redirect()->route('tasks.index')->with([
                'success' => 'Task created successfully.'
            ]);
        } catch (Exception $e) {
            report($e);

            return redirect()->route('tasks.index')->with([
                'error' => 'Failed to create a task.'
            ]);
        }
    }

    public function show(Task $task): View
    {
        $this->authorize('show', $task);

        return view('task.show', compact('task'));
    }

    public function edit(Task $task): View
    {
        $this->authorize('edit', $task);

        $statuses = TaskStatus::cases();

        return view('task.edit', compact('task', 'statuses'));
    }

    public function update(UpdateTaskRequest $request, Task $task): RedirectResponse
    {
        $this->authorize('update', $task);

        try {
            $this->taskService->update($request->validated(), $task);

            return redirect()->route('tasks.show', compact('task'))->with([
                'success' => 'Task updated successfully.'
            ]);
        } catch (Exception $e) {
            report($e);

            return redirect()->route('tasks.index')->with([
                'error' => 'Failed to update the task.'
            ]);
        }
    }

    public function destroy(Task $task): RedirectResponse
    {
        $this->authorize('delete', $task);

        try {
            $this->taskService->delete($task);

            return redirect()->route('tasks.index')->with([
                'success' => 'Task deleted successfully.'
            ]);
        } catch (Exception $e) {
            report($e);

            return redirect()->route('tasks.index')->with([
                'error' => 'Failed to delete the task.'
            ]);
        }
    }
}
