<?php

declare(strict_types=1);

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class TaskFilter extends AbstractFilter
{
    private const TITLE = 'title';
    private const STATUS = 'status';

    protected function getCallbacks(): array
    {
        return [
              self::TITLE => [$this, 'title'],
              self::STATUS => [$this, 'status'],
        ];
    }

    public function title(Builder $builder, string $title): void
    {
        $builder->where('title', 'like', "%$title%");
    }

    public function status(Builder $builder, int $status): void
    {
        $builder->where('status', $status);
    }
}
