<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Http\Filters\TaskFilter;
use App\Models\Task;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class TaskRepository
{
    private const PAGINATE_SIZE = 10;

    public function create(array $data): Task
    {
        return Task::create($data)->refresh();
    }

    public function update(array $data, Task $task): Task
    {
        $task->update($data);

        return $task;
    }

    public function delete(Task $task): void
    {
        $task->delete();
    }

    public function getList(array $filter): LengthAwarePaginator
    {
        return Auth::user()->tasks()
            ->filter(new TaskFilter(array_filter($filter)))
            ->orderByDesc('created_at')->paginate(
                perPage: Arr::get($filter, 'per_page', self::PAGINATE_SIZE),
                page: Arr::get($filter, 'page', 1)
            );
    }

    public function find(int $id): ?Task
    {
        return Task::find($id);
    }
}
