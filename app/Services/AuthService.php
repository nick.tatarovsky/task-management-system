<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\InvalidCredentialsException;
use App\Repositories\UserRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
        //
    }

    /**
     * @throws InvalidCredentialsException
     */
    public function login(array $credentials): void
    {
        if (! Auth::attempt(Arr::only($credentials, ['email', 'password']))) {
            throw new InvalidCredentialsException();
        }

        // Get created user
        $user = $this->userRepository->getUserByEmail(Arr::get($credentials, 'email'));

        auth()->login($user);
    }

    public function logout(): void
    {
        auth()->logout();
    }

    public function register(array $data): void
    {
        $user = $this->userRepository->create($data);

        auth()->login($user);
    }
}
