<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class TaskService
{
    public function __construct(
        private readonly TaskRepository $taskRepository,
    ) {
        //
    }

    public function getList(array $filter): LengthAwarePaginator
    {
        return $this->taskRepository->getList($filter);
    }

    public function store(array $data): Task
    {
        return $this->taskRepository->create(array_merge($data, [
            'user_id' => Auth::id(),
        ]));
    }

    public function update(array $data, Task $task): Task
    {
        return $this->taskRepository->update($data, $task);
    }

    public function delete(Task $task): void
    {
        $this->taskRepository->delete($task);
    }
}
