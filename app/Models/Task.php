<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\Task\TaskStatus;
use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Task extends Model
{
    use Filterable;

    /**
     * @var string
     */
    protected $table = 'tasks';

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id', 'status', 'title', 'description', 'deadline_at'
    ];

    protected $casts = [
        'status' => TaskStatus::class,
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'deadline_at',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
