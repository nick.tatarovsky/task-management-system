<?php

declare(strict_types=1);

namespace App\Enums\Task;

enum TaskStatus: int
{
    case NOT_STARTED = 1;
    case IN_PROGRESS = 2;
    case COMPLETED = 3;

    public function toReadable(): string
    {
        return match ($this) {
            self::NOT_STARTED => 'Not Started',
            self::IN_PROGRESS => 'In Progress',
            self::COMPLETED => 'Completed',
        };
    }
}
