<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        Blade::directive('statusClass', function (mixed $status): string
        {
            return "<?php echo match($status) {
                1 => 'text-warning',
                2 => 'text-primary',
                3 => 'text-success',
            }; ?>";
        });
    }
}
