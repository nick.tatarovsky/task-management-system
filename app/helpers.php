<?php

declare(strict_types=1);

use Illuminate\Support\Carbon;

function formatDatetime(string $date, string $format = 'd.m.Y H:i'): string
{
    return Carbon::make($date)->format($format);
}
