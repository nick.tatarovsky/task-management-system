# Система керування завданнями
## Вимоги
- Docker v3
- Node v14^

## Встановлення
1. Клонуйте віддалений репізоторій на локальну машину
```bash
git clone https://gitlab.com/nick.tatarovsky/task-management-system.git
```
2. Перейдіть до кореневої теки проекту
```bash
cd task-management-system
```
3. Створіть файл .env, скопіювавши файл .env.example
```bash
cp .env.example .env
```
4. Зберіть та запустіть контейнери Docker (У разі необхідності використовуйте `sudo` перед командами Docker для виконання операцій з необхідними привілеями)
```bash
docker-compose up -d
```
5. Встановіть залежності проекту
```bash
docker-compose exec app composer install
```
6. Сгенеруйте ключ застосунку
```bash
docker-compose exec app php artisan key:generate
```
7. Запустіть міграції
```bash
docker-compose exec app php artisan migrate
```
8. Встановіть залежності з package.json
```bash
npm install
```
9. Запустіть проект
```bash
npm run dev
```

## База даних
За замовченням використовується MySQL в контейнері
```bash
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=main
DB_USERNAME=root
DB_PASSWORD=root
```
Після налаштування (за потреби) підключення до бази даних скиньте налаштування оточення
```bash
docker-compose exec app php artisan config:clear
```

## Використання
Перейдіть по http://localhost:8000, створіть аккаунт для взаємодії з завданнями.

При виникненні конфліктів портів з наявними застосунками або службами - змініть їх в [docker-compose.yml](docker-compose.yml)
