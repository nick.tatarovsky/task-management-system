<?php

declare(strict_types=1);

use App\Enums\Task\TaskStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private const TABLE = 'tasks';

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $statuses = Arr::pluck(TaskStatus::cases(), 'value');

        Schema::create(self::TABLE, function (Blueprint $table) use ($statuses) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->enum('status', $statuses)->default(TaskStatus::NOT_STARTED->value);
            $table->string('title', 255);
            $table->text('description');
            $table->dateTime('deadline_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
